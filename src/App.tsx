import React from "react";
import styled from "styled-components";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import { Layout } from "components";
import Adrenalin from "containers/Adrenalin";
import AdrenalinDetails from "containers/Adrenalin/Details";

const Container = styled.div`
  height: 100vh;
  display: grid;
  grid-template-rows: minmax(90px, max-content) 1fr 90px;
  margin: 0 93px;
  @media (max-width: 800px) {
    grid-template-rows: max-content 1fr 90px;
  }
  @media (max-width: 500px) {
    margin: 0 24px;
  }
`;

function App() {
  return (
    <Container>
      <Layout>
        <Router>
          <Switch>
            <Route exact path="/adrenalin" component={Adrenalin} />
            <Route exact path="/adrenalin/:id" component={AdrenalinDetails} />
            <Route exact render={() => <Redirect to="/adrenalin" />} />
          </Switch>
        </Router>
      </Layout>
    </Container>
  );
}

export default App;
