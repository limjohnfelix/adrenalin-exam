import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";
import Text from "../Text";
import { IFeed } from "utils/interface";

const Container = styled.div`
  display: grid;
  height: 517px;
  width: 100%;
  margin: 0 auto;
  grid-template-columns: 1fr 1fr 1fr 1fr;
  grid-template-rows: 1fr 1fr 1fr 1fr 13px;
  @media (max-width: 500px) {
    height: 400px;
  }
`;

const ImageContainer = styled.div<{ thumb: string }>`
  background: #b03532;
  grid-column: 1 / 5;
  grid-row: 1 / 4;
  background: url(${({ thumb }) => thumb});
  background-size: cover;
`;

const PhotoTag = styled.div`
  background: #ffffff;
  grid-column: 1;
  grid-row: 1;
  width: 86px;
  height: 24px;

  transform: rotate(-90deg);
  left: -31px;
  top: 31px;
  position: relative;
  ${Text} {
    top: 5px;
    position: relative;
    right: 40%;
    white-space: nowrap;
    color: #6c6c6c;
    font-size: 11px;
  }
`;

const FooterContainer = styled.div`
  background: #ffffff;
  grid-column: 1 / 4;
  grid-row: 4 / 5;
  margin: 28px 0px;
`;

const CaseContainer = styled.div`
  background: #ffffff;
  grid-column: 1 / 5;
  grid-row: 5 / 6;
  display: flex;
  align-items: center;
`;

const Divider = styled.div`
  border-top: 2px solid #3852f7;
  width: 32px;
  margin-right: 16px;
`;

const Card: React.FC<IFeed> = (props) => {
  const { id, tag, title_long, thumb } = props;
  return (
    <Container>
      <ImageContainer thumb={require(`assets/img/${thumb}`)} />
      <PhotoTag>
        <Text>{tag}</Text>
      </PhotoTag>
      <FooterContainer>
        <Text size={24} fontWeight="bold">
          {title_long}
        </Text>
      </FooterContainer>
      <CaseContainer>
        <Divider />
        <Link to={`/adrenalin/${id}`} style={{ textDecoration: "none" }}>
          <Text color="#3852F7" size={13} fontWeight="bold">
            View Case Study
          </Text>
        </Link>
      </CaseContainer>
    </Container>
  );
};

export default Card;
