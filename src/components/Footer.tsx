import React from "react";
import styled from "styled-components";
import Text from "./Text";
import Logo from "./Logo";

const Container = styled.div`
  display: grid;
  align-content: center;
  justify-items: end;
  width: 100%;
  margin: 0 auto;
  grid-template-columns: 10fr 1fr 1fr 1fr 1fr 1fr 1fr;
  border-top: 2px solid black;
  @media (max-width: 1200px) {
    grid-template-columns: 3fr 1fr 1fr 1fr 1fr 1fr 1fr;
  }
  @media (max-width: 800px) {
    grid-template-columns: 1fr 1fr 1fr 1fr 1fr 1fr 1fr;
  }
  @media (max-width: 650px) {
    grid-template-columns: repeat(1, 1fr);
    justify-items: center;
    grid-gap: 5px;
  }
`;

const StyledLogo = styled(Logo)`
  justify-self: flex-start;
  @media (max-width: 650px) {
    margin-top: 70px;
    justify-self: center;
  }
`;

const Footer = () => {
  return (
    <Container>
      <StyledLogo />
      <Text>Privacy</Text>
      <Text>Sitemap</Text>
      <Text>Facebook</Text>
      <Text>LinkedIn</Text>
      <Text>Instagram</Text>
      <Text>Twitter</Text>
    </Container>
  );
};

export default Footer;
