import React from "react";
import styled from "styled-components";
import Logo from "../Logo";

const Link = styled.a`
  float: left;
  color: #000000;
  padding: 12px;
  text-decoration: none;
  font-size: 13px;
  line-height: 14px;
  font-family: Arial;
`;

const LinkContainer = styled.div<{ isOpen: boolean }>`
  float: right;
  @media screen and (max-width: 800px) {
    display: ${({ isOpen }) => (isOpen ? "block" : "none")};
  }
`;
const ImageContainer = styled.div`
  display: flex;
  float: left;
  justify-content: space-between;
  align-items: center;
`;

const MenuIcon = styled.img`
  height: 30px;
  float: right;
  display: none;
  cursor: pointer;
  @media screen and (max-width: 800px) {
    display: block;
  }
`;

const Navbar = styled.div`
  align-self: center;
  overflow: hidden;
  background-color: #ffffff;
  z-index: 1;
  ${Link} {
    float: left;
  }
  @media screen and (max-width: 800px) {
    position: relative;
    ${Link} {
      float: none;
      display: block;
      text-align: left;
    }
    ${LinkContainer}, ${ImageContainer} {
      float: none;
      margin-top: 30px;
    }
  }
`;

const Header = () => {
  const [isOpen, setOpen] = React.useState(false);

  const handleMenuToggle = () => {
    setOpen((prevState) => !prevState);
  };

  return (
    <Navbar>
      <ImageContainer>
        <Logo padding={16} />
        <MenuIcon
          alt=""
          src={require("assets/icons/menu.png")}
          onClick={handleMenuToggle}
        />
      </ImageContainer>
      <LinkContainer isOpen={isOpen}>
        <Link href="/">Culture</Link>
        <Link href="/">Work</Link>
        <Link href="/">Clients</Link>
        <Link href="/">Services</Link>
        <Link href="/">Careers</Link>
        <Link href="/">Contact</Link>
      </LinkContainer>
    </Navbar>
  );
};

export default Header;
