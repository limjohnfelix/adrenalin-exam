import styled from "styled-components";
import src from "assets/img/adrenalin.svg";

interface ILogo {
  height?: number;
  padding?: number;
}

export default styled.img.attrs({
  src,
  onClick: () => (window.location.href = "/adrenaline"),
})<ILogo>`
  height: ${({ height = 17 }) => height}px;
  padding: ${({ padding = 0 }) => padding}px;
  cursor: pointer;
`;
