import styled from "styled-components";

interface IText {
  size?: number;
  fontFamily?: "Georgia" | "Arial";
  fontWeight?: string;
  color?: string;
}

const Text = styled.p<IText>`
  margin: 0;
  color: ${({ color = "#000000" }) => color};
  font-size: ${({ size = 13 }) => size}px;
  font-family: ${({ fontFamily = "Arial" }) => fontFamily};
  font-weight: ${({ fontWeight = "normal" }) => fontWeight};
`;

export default Text;
