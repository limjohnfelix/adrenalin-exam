import React from "react";
import { RouteComponentProps } from "react-router-dom";
import { Text } from "components";
import styled from "styled-components";
import feeds from "api/feed.json";
import { IFeed } from "utils/interface";

const Container = styled.div`
  display: grid;
  width: 100%;
  margin: 0 auto;
  grid-template-columns: 1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr;
  grid-template-rows: 1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr;
  margin-bottom: 100px;

  @media (max-width: 1000px) {
    grid-template-columns: 1fr;
    grid-template-rows: 1fr 1fr 1fr;
  }
`;

const ImageContainer = styled.div<{ thumb: string }>`
  background: #b03532;
  grid-column: 1 / 5;
  grid-row: 1 / 9;
  background: url(${({ thumb }) => thumb});
  background-size: cover;
  background-position: center center;
  @media (max-width: 900px) {
    grid-column: 1 / 3;
    grid-row: 2 / 2;
  }
`;

const TitleContainer = styled.div`
  grid-column: 6 / 9;
  grid-row: 1 / 3;
  align-self: center;
  @media (max-width: 900px) {
    grid-column: 1 / 3;
    grid-row: 1 / 1;
  }
`;

const QuestionContainer = styled.div`
  grid-column: 6 / 9;
  grid-row: 3 / 9;
  @media (max-width: 900px) {
    grid-column: 1 / 3;
    grid-row: 3 / 3;
  }
`;

const Questions = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  margin-bottom: 24px;
  min-height: 126px;
`;

const Adrenalin: React.FC<RouteComponentProps<{ id: string }>> = ({
  match,
}) => {
  const { id } = match.params;
  const feed: IFeed | undefined = feeds.find(
    (feed) => feed.id === parseInt(id)
  );

  if (!feed) return null;

  return (
    <Container>
      <ImageContainer thumb={require(`assets/img/${feed.image}`)} />
      <TitleContainer>
        <Text size={64} fontWeight="bold">
          {feed.title}
        </Text>
      </TitleContainer>
      <QuestionContainer>
        {feed.questions.map((question, index) => (
          <Questions key={index}>
            <Text size={32} fontWeight="bold">
              Question {index + 1}
            </Text>
            <Text size={21} fontFamily="Georgia">
              {question}
            </Text>
          </Questions>
        ))}
      </QuestionContainer>
    </Container>
  );
};

export default Adrenalin;

// map index is used for demo purposes.
