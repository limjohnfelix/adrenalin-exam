import React from "react";
import styled from "styled-components";
import { Card } from "components";
import feeds from "api/feed.json";
import { IFeed } from "utils/interface";

const Container = styled.div`
  width: 100%;
  margin: 0 auto;
  display: grid;
  grid-gap: 73px 32px;
  @media (min-width: 800px) {
    grid-template-columns: repeat(2, 1fr);
  }
  margin-bottom: 100px;
`;

const Adrenalin = () => {
  const data: IFeed[] = feeds;

  return (
    <Container>
      {data.map((feed) => (
        <Card key={feed.id} {...feed} />
      ))}
    </Container>
  );
};

export default Adrenalin;
